package app.s3amazon;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.VideoView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main2Activity extends AppCompatActivity {
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    private static String file_url = "http://dittotv.live-s.cdn.bitgravity.com/cdn-live/_definst_/dittotv/secure/cartoon_network_Web.smil/chunklist_w1159916351_b56000.m3u8?e=1492986878&a=IN&h=ab6caff00032bad6b746079effcff079&domain=production&key1=dittotv.com%7EChrome%7E57&key2=msisdn%7E00919971622177%7Eyes%7EFTJP2D%7ENA&key3=live%7E";
    Button btnShowProgress;
    VideoView mVideoView;
    int counting = 0 ;
    ImageView my_image;
    Context context;
    File fileToUpload;
    File fileToDownload = new File("/storage/sdcard0/Pictures/MY");
    AmazonS3 s3;
    TransferUtility transferUtility;
    String bucketName = "tv-feeds";
    // File url to download
    String baseUrl = "http://dittotv.live-s.cdn.bitgravity.com/cdn-live/_definst_/dittotv/secure/cartoon_network_Web.smil/";
    String path;
    private Set<String> alreadyFetchedVideos = new HashSet<>();
    // Progress Dialog
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // show progress bar button
        btnShowProgress = (Button) findViewById(R.id.btnProgressBar);
        // Image view to show image after downloading
        my_image = (ImageView) findViewById(R.id.my_image);

        credentialsProvider();
        setTransferUtility();

        // callback method to call the setTransferUtility method


        /**
         *
         * Show Progress bar click event
         * */
        context = this.getApplicationContext();

        btnShowProgress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // starting new Async Task
                new DownloadFileFromURL().execute(file_url);
            }
        });
    }


    /**
     * Showing Dialog
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0

                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading & Uploading file" );
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                counting++;
                return pDialog;
            default:
                return null;
        }
    }

    public void credentialsProvider() {


        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:4f5492df-6ed5-47ea-b91d-37ca2114d0c0", // Identity Pool ID
                Regions.US_WEST_2 // Region
        );
        // Initialize the Amazon Cognito credentials provider


        setAmazonS3Client(credentialsProvider);


    }

    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     *
     * @param credentialsProvider
     */
    public void setAmazonS3Client(CognitoCachingCredentialsProvider credentialsProvider) {

        // Create an S3 client

        s3 = new AmazonS3Client(credentialsProvider);
        s3.setEndpoint("s3-us-west-2.amazonaws.com");


    }


    public void setTransferUtility() {

        transferUtility = new TransferUtility(s3, getApplicationContext());
    }

    public void setFileToUpload(String videoPath,String name) {

        File file = new File(videoPath);
        TransferObserver transferObserver = transferUtility.upload(
                bucketName,     /* The bucket to upload to */
                name,    /* The key for the uploaded object */
                file    /* The file where the data to upload exists */
        );

        transferObserverListener(transferObserver);
    }

    private List<String> parseFileResponse(InputStream response) {

        List<String> videosToFetch = new ArrayList<>();

        BufferedReader br = new BufferedReader(
                new InputStreamReader(response));
        String url;
        try {
            while ((url = br.readLine()) != null) {
                if (url.startsWith("media_")) {
                    url = url + br.readLine();
                    int i = url.indexOf(".ts");
                    if (!alreadyFetchedVideos.contains(url.substring(0, i))) {
                        videosToFetch.add(url);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return videosToFetch;
    }

    public void transferObserverListener(TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.i("statechange", state + "");
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.i("percentage", percentage + "");
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count = 0;
            try {


                while (true)
                {
                    InputStream input = downloadFile(count, f_url);



                    List<String> list = parseFileResponse(input);

                    for (String Vurl : list) {

                        int indexOf = Vurl.indexOf(".ts");
                        alreadyFetchedVideos.add(Vurl.substring(0, indexOf));


                        downloadVideoFile(count, Vurl);

                    }
                }



                // Output stream


            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }


        InputStream downloadFile(int count, String... f_url) throws IOException {
            URL url = new URL(f_url[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            return input;




        }



        void downloadVideoFile(int count, String... f_url) throws IOException {
            URL url = new URL(baseUrl+f_url[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);



            path = "Video" + new Date().getTime() + ".ts";

            OutputStream output = new FileOutputStream("/sdcard" + "/"+path);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();


           String videoPath = Environment.getExternalStorageDirectory().toString() + "/"+path;

            setFileToUpload(videoPath,path);




        }



        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

            // Displaying downloaded image into image view
            // Reading image path from sdcard

            // setting downloaded into image view


            // Intent intent_name = new Intent();
            // intent_name.putExtra("path_image", videoPath);
            // intent_name.putExtra("videoName", path);
            // intent_name.setClass(getApplicationContext(), MainActivity.class);
            // startActivity(intent_name);




        }

    }
}




